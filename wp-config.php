<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'goal1');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'QW@-$EzT,lA.%C2Y.6d<ARaMqGSlUQN2Ns0!Lwjx?*F,c7WzdAqdz|o*zgO6lm^J');
define('SECURE_AUTH_KEY',  'qjW?h!<T$ ]:DaLWuL6UntO(c==9=WzFE}^]pulVQRDr2PJ=|5(C[_CxD`q.6R#H');
define('LOGGED_IN_KEY',    ':fR1#jM3!k3sdPI2,C[~gPyrqaCAZ?m1gkJrG{5=6WH^bbx!u;F75/On_W^-MGKs');
define('NONCE_KEY',        '6!T@tG,F: 8J;kSEBZacNxbC2ZS`~`^ Av]0W -F9<s:fTKGwx5FYfToF^b>B/~*');
define('AUTH_SALT',        '0s=R_3$>tt_,Z^{?<XcR3CM1wCm8k;{}p_^oZbo5c*D$9T1%^-`h_S2z*iX[nAuu');
define('SECURE_AUTH_SALT', 'lx{(/MTw@CaOH_TRViB$iFq{X$ii}#_afoC6/Ho*^,Gz+OAUznQ&rX)sd~E+b=Vi');
define('LOGGED_IN_SALT',   'fUPWM+q,~5j4fPT-!@=b&!qV|caDPv@^a8T2yA6<6:xp$l#a=/,Wsvps&S%A5o>m');
define('NONCE_SALT',       'Bz:x]VYF)}ZS=Y3c&H}cZgd6KI+>E|XXj8]o3je{=cr)2R-vF#<pL-t/*@nKUw>|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
